package ictgradschool.industry.lab12.bounce;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.File;
import java.io.IOException;

/**
 * Created by wasi131 on 28/04/2017.
 */
public class DynamicImageRectangleShape extends DynamicRectangleShape {

    Image image;

    public DynamicImageRectangleShape() {
        loadImage();
    }


    public DynamicImageRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
        loadImage();
    }

    public DynamicImageRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
        loadImage();
    }

    public DynamicImageRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color color) {
        super(x, y, deltaX, deltaY, width, height);
        loadImage();
    }

    private void loadImage() {
        try {
            image = ImageIO.read(new File("william.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void iPaint(GraphicsPainter painter) {
        if (fillColor) {
            painter.drawImage(image, fX,fY,fWidth,fHeight, null);
        }
    }

    @Override
    public void paint(Painter painter) {
        super.paint(painter);
        iPaint((GraphicsPainter) painter);
    }
}
