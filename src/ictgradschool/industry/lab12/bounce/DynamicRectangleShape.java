package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by wasi131 on 28/04/2017.
 */
public class DynamicRectangleShape extends RectangleShape {

    private Color color;
    protected boolean fillColor;
    private int beforeDeltaX;
    private int beforeDeltaY;

    public DynamicRectangleShape() {
        this.color = Color.black;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
        this.color = Color.black;

    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
        this.color = Color.black;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color color) {
        super(x, y, deltaX, deltaY, width, height);
        this.color = Color.black;
    }

    @Override
    public void move(int width, int height) {
        beforeDeltaX = fDeltaX;
        beforeDeltaY = fDeltaY;
        super.move(width, height);
        if (beforeDeltaX != fDeltaX) {
            fillColor = true;
        }
        if (beforeDeltaY != fDeltaY) {
            fillColor = false;
        }
    }

    @Override
    public void paint(Painter painter) {
        if (fillColor) {
            painter.setColor(color);
            painter.fillRect(fX,fY,fWidth,fHeight);
        }

        super.paint(painter);
    }
}
