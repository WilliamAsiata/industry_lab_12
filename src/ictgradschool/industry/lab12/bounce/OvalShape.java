package ictgradschool.industry.lab12.bounce;

/**
 * Created by wasi131 on 28/04/2017.
 */
public class OvalShape extends Shape {
    public OvalShape() {
    }

    public OvalShape(int x, int y) {
        super(x, y);
    }

    public OvalShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public OvalShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {
        painter.drawOval(fX,fY,fWidth,fHeight);
    }
}
