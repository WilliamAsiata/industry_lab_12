package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by wasi131 on 28/04/2017.
 */
public class GemShape extends Shape {
    public GemShape() {
    }

    public GemShape(int x, int y) {
        super(x, y);
    }

    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {
        int[] xPoints;
        int[] yPoints;
        if (fWidth < 20) {
            xPoints = new int[]{fX + fWidth / 2, fX + fWidth, fX + fWidth / 2, fX};
            yPoints = new int[]{fY, fY + fHeight / 2, fY + fHeight, fY + fHeight / 2};
        } else {
            xPoints = new int[]{fX + 20, fX + fWidth - 20, fX, fX + fWidth - 20, fX + 20, fX + fWidth};
            yPoints = new int[]{fY, fY, fY + fHeight / 2, fY + fHeight, fY + fHeight, fY + fHeight / 2};
        }
        painter.drawPolygon(new Polygon(xPoints, yPoints, xPoints.length));
    }
}